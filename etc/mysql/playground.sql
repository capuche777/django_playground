-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-07-2018 a las 05:00:29
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `playground`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add permission', 2, 'add_permission'),
(5, 'Can change permission', 2, 'change_permission'),
(6, 'Can delete permission', 2, 'delete_permission'),
(7, 'Can add group', 3, 'add_group'),
(8, 'Can change group', 3, 'change_group'),
(9, 'Can delete group', 3, 'delete_group'),
(10, 'Can add user', 4, 'add_user'),
(11, 'Can change user', 4, 'change_user'),
(12, 'Can delete user', 4, 'delete_user'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add página', 7, 'add_page'),
(20, 'Can change página', 7, 'change_page'),
(21, 'Can delete página', 7, 'delete_page'),
(22, 'Can add profile', 8, 'add_profile'),
(23, 'Can change profile', 8, 'change_profile'),
(24, 'Can delete profile', 8, 'delete_profile');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$100000$F0iCIOKY8C9o$SVZWv0bKynngMvEc0HXjJlOKW+PLfq1dz+XLiVkfWe8=', '2018-07-28 18:09:14.305462', 1, 'capuche777', '', '', 'django@gmail.com', 1, 1, '2018-07-15 22:50:52.737709'),
(2, 'pbkdf2_sha256$100000$A5T9BFyOGVR1$B65uv1G0qOK8H5GlhoKs+QncU6EKkzzspuzRlSWppOU=', '2018-07-27 17:41:23.526238', 0, 'cloarca', '', '', '', 0, 1, '2018-07-24 03:53:58.000000'),
(3, 'pbkdf2_sha256$100000$QNBf1hj06jfZ$KyXQ2wwwmwGQwAr4p+POsroCu+V9YdrlcnnGfeZndBY=', '2018-07-26 19:54:01.015124', 0, 'test1', '', '', '', 0, 1, '2018-07-26 19:53:45.362943'),
(4, 'pbkdf2_sha256$100000$f4lsOkmbirEJ$0YjVa2d2XMpkGjN297YE9xcyv9XLsigoHL/9bqgY878=', '2018-07-26 20:04:57.377087', 0, 'test2', '', '', '', 0, 1, '2018-07-26 20:04:49.431848'),
(5, 'pbkdf2_sha256$100000$zEarpfZE61WF$EhZNx57smRHR3BxHCxe4rneQDI3x8eUAaY5Jfy2nyVs=', '2018-07-26 21:53:48.030003', 0, 'test3', '', '', 'test3@gmail.com', 0, 1, '2018-07-26 21:17:40.195860'),
(6, 'pbkdf2_sha256$100000$SWF7veodIqZT$EG/3UM/eWexjMuS+BHvCcrEM8rFvu9phQ67MbPWbifU=', NULL, 0, 'test6', '', '', 'test6@gmail.com', 0, 1, '2018-07-27 23:21:14.753388'),
(7, 'pbkdf2_sha256$100000$CKvlDvKiCrbM$YZ9EJJxz/kXwgCEOtKpndsBKrtHrsubal/++bGm+k2k=', NULL, 0, 'test', '', '', 'test7@gmail.com', 0, 1, '2018-07-27 23:22:10.174504'),
(8, 'pbkdf2_sha256$100000$aKpvBeSVkDry$Cynzjy2vUGDNN510Kwql8XlQVD2dNWrac2P686v8r1M=', '2018-07-27 23:23:45.294957', 0, 'test8', '', '', 'test8@gmail.com', 0, 1, '2018-07-27 23:23:31.097048');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2018-07-15 22:59:43.393312', '1', 'Lorem ipsum dolor', 1, '[{\"added\": {}}]', 7, 1),
(2, '2018-07-15 23:01:42.401980', '2', 'Praesent eget rutrum magna', 1, '[{\"added\": {}}]', 7, 1),
(3, '2018-07-24 03:53:59.409214', '2', 'cloarca', 1, '[{\"added\": {}}]', 4, 1),
(4, '2018-07-24 03:54:08.117098', '2', 'cloarca', 2, '[]', 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(7, 'pages', 'page'),
(8, 'registration', 'profile'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2018-07-15 03:26:38.045188'),
(2, 'auth', '0001_initial', '2018-07-15 03:26:49.361681'),
(3, 'admin', '0001_initial', '2018-07-15 03:26:51.584850'),
(4, 'admin', '0002_logentry_remove_auto_add', '2018-07-15 03:26:51.656873'),
(5, 'contenttypes', '0002_remove_content_type_name', '2018-07-15 03:26:52.868797'),
(6, 'auth', '0002_alter_permission_name_max_length', '2018-07-15 03:26:53.935909'),
(7, 'auth', '0003_alter_user_email_max_length', '2018-07-15 03:26:55.756906'),
(8, 'auth', '0004_alter_user_username_opts', '2018-07-15 03:26:55.912915'),
(9, 'auth', '0005_alter_user_last_login_null', '2018-07-15 03:26:56.639160'),
(10, 'auth', '0006_require_contenttypes_0002', '2018-07-15 03:26:56.698203'),
(11, 'auth', '0007_alter_validators_add_error_messages', '2018-07-15 03:26:56.779242'),
(12, 'auth', '0008_alter_user_username_max_length', '2018-07-15 03:26:57.562046'),
(13, 'auth', '0009_alter_user_last_name_max_length', '2018-07-15 03:26:58.764511'),
(14, 'sessions', '0001_initial', '2018-07-15 03:27:00.584792'),
(15, 'pages', '0001_initial', '2018-07-15 22:48:26.890669'),
(16, 'registration', '0001_initial', '2018-07-27 16:53:16.055305');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('8lwvkaqhrszjf79kqsye9iqr0l186ry0', 'ODI4ZmYxNzdlYTFjMjUzMDE2ZmQ2MGJiMDk2MDM2OWQxMTIyNGVmODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxNGI0MDQyNWNjYjE3NjNmMDZkNWYwZGQ1MzZjYzZkYzQzZTAzYTFkIn0=', '2018-07-29 22:56:34.506968'),
('8z0megv68ema789wcj17xxvwq4f0iwa6', 'ODI4ZmYxNzdlYTFjMjUzMDE2ZmQ2MGJiMDk2MDM2OWQxMTIyNGVmODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxNGI0MDQyNWNjYjE3NjNmMDZkNWYwZGQ1MzZjYzZkYzQzZTAzYTFkIn0=', '2018-08-03 15:38:52.115046'),
('9p1q82r5kxob1tu71mfw6cdlrim3rkgx', 'ODI4ZmYxNzdlYTFjMjUzMDE2ZmQ2MGJiMDk2MDM2OWQxMTIyNGVmODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxNGI0MDQyNWNjYjE3NjNmMDZkNWYwZGQ1MzZjYzZkYzQzZTAzYTFkIn0=', '2018-08-02 15:31:49.618914'),
('h7ejcosgrga1xytd1vyogxqbhxfpnhdq', 'ODI4ZmYxNzdlYTFjMjUzMDE2ZmQ2MGJiMDk2MDM2OWQxMTIyNGVmODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxNGI0MDQyNWNjYjE3NjNmMDZkNWYwZGQ1MzZjYzZkYzQzZTAzYTFkIn0=', '2018-08-02 17:39:38.948678'),
('h9qodxdh9l2gstjomwc8wu24kd0nv2bu', 'ODI4ZmYxNzdlYTFjMjUzMDE2ZmQ2MGJiMDk2MDM2OWQxMTIyNGVmODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxNGI0MDQyNWNjYjE3NjNmMDZkNWYwZGQ1MzZjYzZkYzQzZTAzYTFkIn0=', '2018-08-01 15:55:21.917691'),
('ksdvj0jnbjjnf8wps97zh0nsiwz2eij3', 'ODI4ZmYxNzdlYTFjMjUzMDE2ZmQ2MGJiMDk2MDM2OWQxMTIyNGVmODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxNGI0MDQyNWNjYjE3NjNmMDZkNWYwZGQ1MzZjYzZkYzQzZTAzYTFkIn0=', '2018-08-11 18:09:14.417493'),
('kuxxhq0i768sa2cby97rnc8ghg74sgwr', 'MzYwMWJhNDU5NjkyNGI1OTc5ZjNjNTNhMWIwNWI2OWQwOWNkMzNiODp7Il9hdXRoX3VzZXJfaWQiOiI1IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZTcwYjMyMjI5YzU5NjBlZmQ5OTgwZDkzNjQ2ZTA1NTM0OTYzNzQ5In0=', '2018-08-09 21:53:48.103842'),
('lnji71lw1u45ucaf1p5kfk8jy2zpzs7k', 'MjBlZmI3ZDA3OWYwY2RjNDAxNjkyNzAzYWE1YzQyNGJiMDVhYThhOTp7Il9hdXRoX3VzZXJfaWQiOiI4IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1N2NkMWZmNjUyYTI4NWU3YWM0ODI0MWU3N2NjY2I5MzBmMmUxYzBiIn0=', '2018-08-10 23:23:45.417950'),
('zu5g5r35g3iu1fwg7a6tefyet8bren99', 'ODI4ZmYxNzdlYTFjMjUzMDE2ZmQ2MGJiMDk2MDM2OWQxMTIyNGVmODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxNGI0MDQyNWNjYjE3NjNmMDZkNWYwZGQ1MzZjYzZkYzQzZTAzYTFkIn0=', '2018-08-09 19:56:03.179570');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pages_page`
--

CREATE TABLE `pages_page` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` longtext NOT NULL,
  `order` smallint(6) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pages_page`
--

INSERT INTO `pages_page` (`id`, `title`, `content`, `order`, `created`, `updated`) VALUES
(2, 'Praesent eget rutrum magna', '<p>Praesent eget rutrum magna, convallis faucibus mi. Etiam molestie augue in mauris mollis, eu eleifend quam luctus. In imperdiet fermentum blandit. Sed id nunc in risus pretium viverra. Mauris eget nunc sit amet odio suscipit bibendum sit amet ac arcu. Ut et elit urna. Donec placerat orci at magna ultricies, a posuere turpis sodales. Cras porta risus euismod arcu dictum pulvinar. Donec imperdiet, elit nec sollicitudin dignissim, eros erat porta lacus, vel faucibus dolor neque eu massa. Mauris dictum lectus et pulvinar tempus. Ut consequat, nisi a ultrices consequat, nisi felis consectetur tellus, eget porta justo massa vel nibh.</p>', 0, '2018-07-15 23:01:42.383980', '2018-07-15 23:01:42.383980'),
(3, 'Testeando', '<p>Editando el contenido de la pagina, a ver y no al cine</p>', 1, '2018-07-18 18:01:30.826025', '2018-07-20 05:07:11.880553');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registration_profile`
--

CREATE TABLE `registration_profile` (
  `id` int(11) NOT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `bio` longtext,
  `link` varchar(200) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `registration_profile`
--

INSERT INTO `registration_profile` (`id`, `avatar`, `bio`, `link`, `user_id`) VALUES
(1, '', 'Testeando', 'http://www.google.com', 1),
(2, '', 'Test', 'http://www.google.com', 2),
(3, '', NULL, NULL, 8);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indices de la tabla `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indices de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indices de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indices de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indices de la tabla `pages_page`
--
ALTER TABLE `pages_page`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `registration_profile`
--
ALTER TABLE `registration_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `pages_page`
--
ALTER TABLE `pages_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `registration_profile`
--
ALTER TABLE `registration_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Filtros para la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Filtros para la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `registration_profile`
--
ALTER TABLE `registration_profile`
  ADD CONSTRAINT `registration_profile_user_id_e133ce43_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
